package model.data_structuresTest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.QueueList;

public class QueueTest< T extends Comparable<T>>{
	
	private QueueList<T> lista;
	private T elemento1;
	private T elemento2; 
	private T elemento3;
	
	@Before
	public void testSetUp()
	{
		lista = new QueueList<T>();
	}
	
	public void testSetUp2()
	{
		lista = new QueueList<T>();
		lista.enqueue(elemento1);
		lista.enqueue(elemento2);
		lista.enqueue(elemento3);
	}
	
	@Test
	public void QueueListTest()
	{
		testSetUp();
		assertEquals("No se inicializa la lista de cola correctamente", lista, lista);
	}

	@Test
	public void getSizeTest()
	{
		testSetUp2();
		assertEquals("No se retorna el tamaNo correcto de la LinkedList de cola", 3, lista.size());
	}
	
	@Test
	public void enqueueTest()
	{
		testSetUp();
		assertEquals("No se añade correctamente el elemento", 0, lista.size());
		lista.enqueue(elemento2);
		assertEquals("No se añade correctamente el elemento", 1, lista.size());
	}
	
	@Test 
	public void dequeueTest()
	{
		testSetUp2();
		lista.dequeue();
		assertEquals("No se elimina correctamente el elemento", 2, lista.size());
	}
	
	@Test
	public void isEmptyTest()
	{
		testSetUp2();
		assertEquals("No indica correctamente que la lista esta o no vacia", false,lista.isEmpty());
		testSetUp();
		assertEquals("No indica correctamente que la lista esta o no vacia" , true, lista.isEmpty());
	}

}
