package model.data_structuresTest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.StackList;

public class StackTest<T extends Comparable<T>> {
	
	
	private StackList<T> lista;
	private T elemento1;
	private T elemento2;
	private T elemento3;
	
	@Before
	public void testSetUp()
	{
		lista = new StackList<T>();
	}
	
	@Before
	public void testSetUp2()
	{
		lista = new StackList<T>();
		lista.push(elemento1);
		lista.push(elemento2);
		lista.push(elemento3);
	}
	
	@Test
	public void StackTestList()
	{
		testSetUp();
		assertEquals("No se inicializa la lista de pila correctamente", lista, lista);
	}
	
	@Test
	public void pushTest()
	{
		testSetUp();
		assertEquals("El elemento no se añade correctamente", 0, lista.size());
		lista.push(elemento2);
		assertEquals("El elemento no se añade correctamente", 1, lista.size());
	}
	
	@Test
	public void popTest()
	{
		testSetUp2();
		lista.pop();
		assertEquals("El elemento no se elimina correctamente", 2, lista.size());
	}
	
	@Test
	public void sizeTest()
	{
		testSetUp2();
		assertEquals("No se retorna el tamaNo correcto de la LinkedList de pila", 3, lista.size());
	}

	@Test
	public void isEmptyTest()
	{
		testSetUp2();
		assertEquals("No indica correctamente que la lista esta o no vacia", false,lista.isEmpty());
		testSetUp();
		assertEquals("No indica correctamente que la lista esta o no vacia" , true, lista.isEmpty());
	}
	
	
	 
}
