package controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.Stack;
import model.data_structures.NodeList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.QueueList;
import model.data_structures.StackList;
import model.logic.MovingViolations;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;
	public final static String no = "No";


	public final static String mesEnero = "./data/Moving_Violations_Issued_in_January_2018.csv";

	public final static String mesFebrero = "./data/Moving_Violations_Issued_in_February_2018.csv";

	public final static String mesMarzo = "./data/Moving_Violations_Issued_in_March_2018.csv";

	public final static String mesAbril = "./data/Moving_Violations_Issued_in_April_2018.csv";

	public final static String mesMayo = "./data/Moving_Violations_Issued_in_May_2018.csv";

	public final static String mesJunio = "./data/Moving_Violations_Issued_in_June_2018.csv";

	public final static String mesJulio = "./data/Moving_Violations_Issued_in_July_2018.csv";

	public final static String mesAgosto = "./data/Moving_Violations_Issued_in_August_2018.csv";

	public final static String mesSeptiembre = "./data/Moving_Violations_Issued_in_September_2018.csv";

	public final static String mesOctubre = "./data/Moving_Violations_Issued_in_October_2018.csv";

	public final static String mesNomviembre = "./data/Moving_Violations_Issued_in_November_2018.csv";

	public final static String mesdiciembre= "./data/Moving_Violations_Issued_in_December_2018.csv";

	private MovingViolations movingViolation;

	LinkedList<VOMovingViolations> moving; 

	private int numeroCuatrimestre;

	public Controller() {

		view = new MovingViolationsManagerView();
		movingViolation = new MovingViolations();
		moving =  movingViolation.getMoving();
		numeroCuatrimestre=0;
	}
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();
		while(!fin)
		{
			view.printMenu();
			int option = sc.nextInt();
			switch(option)
			{
			case 0:
				view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
				numeroCuatrimestre = sc.nextInt();
				controller.loadMovingViolations(numeroCuatrimestre);
				break;


			case 1:
				boolean respuesta = controller.verifyObjectIDIsUnique();
				view.printMessage("Repetidos = "  + respuesta);
				break;
			case 2:

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2018-01-26T23:35:00.000Z )");
				LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next());
				view.printMessage("Ingrese la fecha con hora final (Ej : 2018-01-26T23:35:00.000Z)");
				LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next());
				IQueue<VOMovingViolations> resultados2 = controller.getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);
				view.printMovingViolationsReq2(resultados2);
				break;

			case 3:
				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode3 = sc.next();
				double [] promedios3 = controller.avgFineAmountByViolationCode(violationCode3);
				view.printMessage("FINEAMT promedio sin accidente: " + promedios3[0] + ", con accidente:" + promedios3[1]);
				break;
			case 4:
				view.printMessage("Ingrese el ADDRESS_ID");
				String addressId4 = sc.next();

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2018-03-26)");
				LocalDate fechaInicialReq4A = convertirFecha(sc.next());
				view.printMessage("Ingrese la fecha con hora final (Ej : 2018-03-26)");
				LocalDate fechaFinalReq4A = convertirFecha(sc.next());
				IStack<VOMovingViolations> resultados4 = controller.getMovingViolationsAtAddressInRange(addressId4, fechaInicialReq4A, fechaFinalReq4A);
				view.printMovingViolationsReq4(resultados4);
				break;

			case 5:
				view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
				double limiteInf5 = sc.nextDouble();
				view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
				double limiteSup5 = sc.nextDouble();
				IQueue<VOViolationCode> violationCodes = controller.violationCodesByFineAmt(limiteInf5, limiteSup5);
				view.printViolationCodesReq5(violationCodes);
				break;
			case 6:
				view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
				double limiteInf6 = sc.nextDouble();
				view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
				double limiteSup6 = sc.nextDouble();
				view.printMessage("Ordenar Ascendentmente: (Ej: true)");
				boolean ascendente6 = sc.nextBoolean();
				IStack<VOMovingViolations> resultados6 = controller.getMovingViolationsByTotalPaid(limiteInf6, limiteSup6, ascendente6);
				view.printMovingViolationReq6(resultados6);
				break;
			case 7:
				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial7 = sc.nextInt();
				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal7 = sc.nextInt();
				IQueue<VOMovingViolations> resultados7 = controller.getMovingViolationsByHour(horaInicial7, horaFinal7);
				view.printMovingViolationsReq7(resultados7);
				break;
			case 8:
				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode8 = sc.next();
				double [] resultado8 = controller.avgAndStdDevFineAmtOfMovingViolation(violationCode8);
				view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviaciÃ³n estandar:" + resultado8[1]);
				break;
			case 9:
				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial9 = sc.nextInt();
				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal9 = sc.nextInt();
				int resultado9 = controller.countMovingViolationsInHourRange(horaInicial9, horaFinal9);
				view.printMessage("NÃºmero de infracciones: " + resultado9);
				break;
			case 10:
				//view.printMovingViolationsByHourReq10();
				controller.graficaASCCIporcentaje();
				break;
			case 11:
				view.printMessage("Ingrese la fecha inicial (Ej : 2018-03-26)");
				LocalDate fechaInicial11 = convertirFecha(sc.next());
				view.printMessage("Ingrese la fecha final (Ej : 2018-03-26)");
				LocalDate fechaFinal11 = convertirFecha(sc.next());
				double resultados11 = controller.totalDebt(fechaInicial11, fechaFinal11);
				view.printMessage("Deuda total "+ resultados11);
				break;
			case 12: 
				String[] meses= controller.graficaFine();
				view.printTotalDebtbyMonthReq12(meses[0],meses[1], meses[2],meses[3]);
				System.out.println("Cada X representa $100 USD");
				break;
			case 13: 
				fin=true;
				sc.close();
				break;
			case 16:
				view.printDailyStatistics(controller.getDailyStatistics());
			}
		}

	}
	public void loadMovingViolations(int numeroCuatrimestre) {
		if( numeroCuatrimestre == 1)
		{
			movingViolation.loadMovingViolations(mesEnero, false);
			movingViolation.loadMovingViolations(mesFebrero, false);
			movingViolation.loadMovingViolations(mesMarzo, false);
			movingViolation.loadMovingViolations(mesAbril, false);
		}
		else if( numeroCuatrimestre == 2)
		{
			movingViolation.loadMovingViolations(mesMayo, false);
			movingViolation.loadMovingViolations(mesJunio, false);
			movingViolation.loadMovingViolations(mesJulio, false);
			movingViolation.loadMovingViolations(mesAgosto, false);
		}
		else if( numeroCuatrimestre == 3)
		{
			movingViolation.loadMovingViolations(mesSeptiembre, false);
			movingViolation.loadMovingViolations(mesOctubre, true);
			movingViolation.loadMovingViolations(mesNomviembre, true);
			movingViolation.loadMovingViolations(mesdiciembre, true);
		}
	}
	public IQueue <VODaylyStatistic> getDailyStatistics () {
		System.out.println(moving.getSize());
		/*for (int i = 0; i < moving.getSize(); i++) {
			System.out.println(moving.getElement(i));
		}*/
		return movingViolation.getDailyStatistics();
	}
	public IStack <VOMovingViolations> nLastAccidents(int n) {
		return movingViolation.nLastAccidents(n);
	}




	//A1 Angela Suarez
	public boolean verifyObjectIDIsUnique() {
		return movingViolation.verificar();
	}

	//A2   Angela Suarez
	public IQueue<VOMovingViolations> getMovingViolationsInRange(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {

		IQueue<VOMovingViolations> cola = new QueueList<>();

		LinkedList<VOMovingViolations> lista = new LinkedList<>();

		for (int i = 0; i < moving.getSize(); i++) 
		{

			LocalDateTime fechaConvertida = convertirFecha_Hora_LDT(movingViolation.getMoving().getElement(i).getTicketIssueDate());

			if( fechaConvertida.getMonthValue() >= fechaInicial.getMonthValue()      )
			{


				if( fechaConvertida.compareTo(fechaInicial) > 0 && fechaConvertida.compareTo(fechaFinal)<0)
				{
					lista.addInorder(moving.getElement(i));
				}
			}
			else
			{
				break;
			}


		}
		if( !lista.isEmpty())
		{
			for (int i = 0; i < lista.getSize(); i++) {
				cola.enqueue(lista.getElement(i));
			}
		}
		return cola;
	}

	//A3 Angela Suarez
	public double[] avgFineAmountByViolationCode(String violationCode3) {
		double fineamt = 0.0;
		double fineamt2 = 0.0;
		int cantidad = 0;
		int cantidad2 = 0;
		for( int i = 0 ; i < moving.getSize() ; i ++)
		{
			if( moving.getElement(i).getViolationCode().equals(violationCode3))
			{
				if (moving.getElement(i).getAccidentIndicator().equals(no))
				{ 
					cantidad++;
					fineamt += moving.getElement(i).getFine();
				}
				else
				{
					cantidad2++;
					fineamt2 += moving.getElement(i).getFine();
				}
			}
		}

		double noTiene = fineamt/cantidad;
		double Sitieme = fineamt2/cantidad2;
		return new double [] {noTiene , Sitieme};
	}

	//A4 Angela Suarez
	public IStack<VOMovingViolations> getMovingViolationsAtAddressInRange(String addressId,
			LocalDate fechaInicial, LocalDate fechaFinal) {

		IStack<VOMovingViolations> pila = new StackList<>();
		LinkedList<VOMovingViolations> lista = new LinkedList<>();
		for (int i = 0; i < moving.getSize(); i++) 
		{
			String fechita = moving.getElement(i).getTicketIssueDate(); 
			fechita = fechita.substring(0, fechita.indexOf("T"));
			LocalDate fechaConvertida = convertirFecha(fechita);


			if( fechaConvertida.getMonthValue()  >= fechaInicial.getMonthValue()      )
			{

				if (fechaConvertida.compareTo(fechaInicial) > 0 && fechaConvertida.compareTo(fechaFinal)<0 && moving.getElement(i).getAddressId().equals(addressId))
				{
					lista.addInorder(moving.getElement(i));
				}
			}

			else
			{
				break;
			}

		}


		if( !lista.isEmpty())
		{
			for (int i = 0; i < lista.getSize(); i++) {
				pila.push(lista.getElement(i));
			}
		}
		return pila;
	}

	//1B Sofia G
	public IQueue<VOViolationCode> violationCodesByFineAmt(double limiteInf5, double limiteSup5) {
		// TODO Auto-generated method stub
		IQueue<VOViolationCode> resp= new QueueList<VOViolationCode>();
		IQueue<VOMovingViolations> movingQueue=movingViolation.getQueue();
		System.out.println(movingQueue.size());
		VOMovingViolations actual=movingQueue.dequeue();

		while(!movingQueue.isEmpty())
		{
			double promedio=0;
			int total=0;
			String descripActual="";
			VOMovingViolations siguiente=movingQueue.dequeue();
			while(actual!=null&& siguiente!=null)
			{
				descripActual=actual.getViolationDescription();
				String descripSig=siguiente.getViolationDescription();
				if(descripActual.equalsIgnoreCase(descripSig))
				{
					promedio+=actual.getFine();
					total++;
				}
				else
				{
					movingQueue.enqueue(siguiente);
				}
				actual=movingQueue.dequeue();
			}
			promedio=promedio/total;
			if(limiteInf5<promedio&& promedio<limiteSup5)
			{
				VOViolationCode t= new VOViolationCode(descripActual, promedio);
				resp.enqueue(t);
			}			

			actual=movingQueue.dequeue();
		}

		return resp;
	}
	//2B 
	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6,
			boolean ascendente6) {
		StackList<VOMovingViolations> resp= new StackList<VOMovingViolations>();
		NodeList<VOMovingViolations> actual=moving.getFirstNode();

		while(actual!=null)
		{
			double total=actual.getelem().getTotalPaid();
			if(limiteInf6<total&& total<limiteSup6)
			{
				VOMovingViolations.ComparadorXfecha compXfecha= new VOMovingViolations.ComparadorXfecha();
				resp.pushConCompare(actual.getelem(), compXfecha);
				//resp.push(actual.getelem());
			}
			actual=actual.getNext();
		}

		if(ascendente6)
		{return resp;}
		else 
		{
			QueueList<VOMovingViolations> nueva= new QueueList<VOMovingViolations>();
			while(!resp.isEmpty())
			{
				VOMovingViolations t= resp.pop();
				nueva.enqueue(t);
			}
			while(!nueva.isEmpty())
			{
				VOMovingViolations t= nueva.dequeue();
				resp.push(t);
			}
			return resp;}
	}
	//3B
	public IQueue<VOMovingViolations> getMovingViolationsByHour(int horaInicial7, int horaFinal7) {
		// TODO Auto-generated method stub
		IQueue<VOMovingViolations> resp= new QueueList<VOMovingViolations>();
		NodeList<VOMovingViolations> actual=moving.getFirstNode();

		while(actual!=null)
		{
			int hora=convertirFecha_Hora_LDT(actual.getelem().getTicketIssueDate()).getHour();
			//El rango va abierto en el extremo final, es decir si la horafinal es 3, se muestran hasta las 2:59
			if(horaInicial7<hora&& hora<horaFinal7-1)
			{
				resp.enqueue(actual.getelem());
			}
			actual=actual.getNext();
		}
		return resp;
	}
	//4B
	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8) {
		// TODO Auto-generated method stub
		double promedio=0.0;
		double	desviacion=0.0;
		int n=0;
		NodeList<VOMovingViolations> actual=moving.getFirstNode();
		LinkedList<Double> fines= new LinkedList<Double>();

		while(actual!=null)
		{
			if(actual.getelem().getViolationCode().equalsIgnoreCase(violationCode8))
			{
				promedio+=actual.getelem().getFine();
				n++;
				fines.add(actual.getelem().getFine());
			}
			actual=actual.getNext();
		}
		promedio=(double)promedio/n;

		NodeList<Double> actualfine=fines.getFirstNode();
		double diferencia=0.0;
		while(actualfine!=null)
		{
			diferencia+=(actualfine.getelem()-promedio)*(actualfine.getelem()-promedio);
			actualfine=actualfine.getNext();
		}
		desviacion=Math.sqrt(diferencia/n);
		return new double [] {promedio , desviacion};
	}

	//C9
	public int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9) {

		int count =0;
		for( int i = 0 ; i < moving.getSize() ; i ++)
		{

			String fechita = moving.getElement(i).getTicketIssueDate(); 
			LocalDateTime fea = convertirFecha_Hora_LDT(fechita);



			if( fea.getHour() > horaInicial9 && fea.getHour() < horaFinal9   )
			{
				count++;
			}

		}
		return count;
	}

	//C11
	public double totalDebt(LocalDate fechaInicial11, LocalDate fechaFinal11) {
		double ha = 0.0;
		double todoPagar = 0.0;
		double h = 0.0;
		for (int i = 0; i < moving.getSize(); i++) 
		{
			String fechita = moving.getElement(i).getTicketIssueDate(); 
			fechita = fechita.substring(0, fechita.indexOf("T"));
			LocalDate fechaConvertida = convertirFecha(fechita);


			if( fechaConvertida.getMonthValue()  >= fechaInicial11.getMonthValue()      )
			{

				if (fechaConvertida.compareTo(fechaInicial11) > 0 && fechaConvertida.compareTo(fechaFinal11)<0 )
				{
					if( moving.getElement(i).getPenalty1().equals("")  )
					{
						h = 0.0;

					}

					else
					{
						ha = Double.parseDouble(moving.getElement(i).getPenalty1());
					}

					if( moving.getElement(i).getPenalty2().equals("")  )
					{
						h = 0.0;
					}

					else
					{
						ha = Double.parseDouble(moving.getElement(i).getPenalty2());
					}

					todoPagar += moving.getElement(i).getFine() + moving.getElement(i).getTotalPaid() + ha + h;
				}



			}
			else
			{
				break;
			}



		}

		return todoPagar;
	}


	// C10
	public void graficaASCCIporcentaje()
	{


		long contador0 = 0;
		long contador1 = 0;
		long contador2 = 0;
		long contador3 = 0;
		long contador4 = 0;
		long contador5 = 0;
		long contador6 = 0;
		long contador7 = 0;
		long contador8 = 0;
		long contador9 = 0;
		long contador10 = 0;
		long contador11 = 0;
		long contador12 = 0;
		long contador13 = 0;
		long contador14 = 0;
		long contador15 = 0;
		long contador16 = 0;
		long contador17 = 0;
		long contador18 = 0;
		long contador19 = 0;
		long contador20 = 0;
		long contador21 = 0;
		long contador22 = 0;
		long contador23 = 0;
		long contador24 = 0;
		long total =0;

		for ( int i = 0 ; i < moving.getSize() ; i ++)
		{

			String fechita = moving.getElement(i).getTicketIssueDate(); 
			LocalDateTime fechaTotal = convertirFecha_Hora_LDT(fechita);


			if ( !moving.getElement(i).getAccidentIndicator().equals("No")   )
			{
				if( fechaTotal.getHour() == 0)
				{  
					total++; 
					contador0++;
				}
				else if ( fechaTotal.getHour() == 1)
				{               
					total++; 
					contador1++; 
				}
				else if ( fechaTotal.getHour() == 2)
				{ total++;
				contador2++;                    }
				else if ( fechaTotal.getHour() == 3)
				{
					total++; contador3++;
				}
				else if ( fechaTotal.getHour() == 4)
				{
					total++; contador4++;
				}
				else if ( fechaTotal.getHour() == 5)
				{
					total++; contador5++;
				}
				else if ( fechaTotal.getHour() == 6)
				{
					total++; contador6++;
				}
				else if ( fechaTotal.getHour() == 7)
				{
					total++; contador7++;
				}
				else if ( fechaTotal.getHour() == 8)
				{
					total++; contador8++;
				}
				else if ( fechaTotal.getHour() == 9)
				{
					total++; contador9++;
				}
				else if ( fechaTotal.getHour() == 10)
				{total++; contador10++;
				}
				else if ( fechaTotal.getHour() == 11)
				{total++; contador11++;
				}
				else if ( fechaTotal.getHour() == 12)
				{total++; 
				contador12++;}
				else if ( fechaTotal.getHour() == 13)
				{
					total++; contador13++;
				}
				else if ( fechaTotal.getHour() == 14)
				{ total++; 
				contador14++; }
				else if ( fechaTotal.getHour() == 15)
				{ total++;
				contador15++;}
				else if ( fechaTotal.getHour() == 16)
				{ total++;
				contador16++;}
				else if ( fechaTotal.getHour() == 17)
				{total++; 
				contador17++; }
				else if ( fechaTotal.getHour() == 18)
				{total++;
				contador18++; }
				else if ( fechaTotal.getHour() == 19)
				{total++; 
				contador19++; }
				else if ( fechaTotal.getHour() == 20)
				{total++; 
				contador20++;}
				else if ( fechaTotal.getHour() == 21)
				{total++;
				contador21++;}
				else if ( fechaTotal.getHour() == 22)
				{total++;
				contador22++;}
				else if ( fechaTotal.getHour() == 23)
				{total++;
				contador23++;}

			}


		}

		System.out.println("Porcentaje de infracciones que tuvieron accidentes por hora. 2018");
		System.out.println("Hora| % de accidentes");
		System.out.println("00 |" +  (double)( contador0*100)/total + " %");
		System.out.println("01 |" + (double)( contador1*100)/total + " %");
		System.out.println("02 |" + (double)( contador2*100)/total + " %" );
		System.out.println("03 |" + (double)( contador3*100)/total + " %");
		System.out.println("04 |" + (double)( contador4*100)/total + " %");
		System.out.println("05 |" + (double)( contador5*100)/total + " %");
		System.out.println("06 |" + (double)( contador6*100)/total + " %");
		System.out.println("07 |" + (double)( contador7*100)/total + " %");
		System.out.println("08 |" + (double)( contador8*100)/total + " %");
		System.out.println("09 |" + (double)( contador9*100)/total + " %");
		System.out.println("10 |" + (double)( contador10*100)/total + " %" );
		System.out.println("11 |" + (double)( contador11*100)/total + " %" );
		System.out.println("12 |" + (double)( contador12*100)/total + " %");
		System.out.println("13 |" + (double)( contador13*100)/total + " %" );
		System.out.println("14 |" + (double)( contador14*100)/total + " %" );
		System.out.println("15 | " + (double)( contador15*100)/total + " %" );
		System.out.println("16 | " + (double)( contador16*100)/total + " %");
		System.out.println("17 | " + (double)( contador17*100)/total + " %");
		System.out.println("18 | " + (double)( contador18*100)/total + " %");
		System.out.println("19 | " + (double)( contador19*100)/total + " %");
		System.out.println("20 | " + (double)( contador20*100)/total + " %");
		System.out.println("21 | " + (double)( contador21*100)/total + " %");
		System.out.println("22 | " + (double)( contador22*100)/total + " %");
		System.out.println("23 | " + (double)( contador23*100)/total + " %");
		System.out.println("");
		System.out.println("Cada numero representa un porcentaje%");
	}



	//C12 Grafica
	public String[] grafica2()
	{
		String[] resp=new String[4];
		String[] inicio=new String[4];
		String[] fin=new String[4];
		resp[0]="";
		resp[1]="";
		resp[2]="";
		resp[3]="";
		switch(numeroCuatrimestre)
		{
		case 1:
			Double total1=0.0;
			inicio[0]="2018-01-01";
			fin[0]="2018-01-31";
			total1=totalDebt(convertirFecha(inicio[0]),convertirFecha(fin[0]))/100;
			Double i=total1;
			while(i>0.0)
			{
				resp[0]+="X";
				i--;
			}


			inicio[1]="2018-02-01";
			fin[1]="2018-02-28";
			total1+=totalDebt(convertirFecha(inicio[1]),convertirFecha(fin[1]))/100;
			Double j=total1;
			while(j>0.0)
			{
				resp[1]+="X";
				j--;
			}

			inicio[2]="2018-03-01";
			fin[2]="2018-03-31";
			total1+=totalDebt(convertirFecha(inicio[2]),convertirFecha(fin[2]))/100;
			Double k=total1;
			while(k>0.0)
			{
				resp[2]+="X";
				k--;
			}

			inicio[3]="2018-04-01";
			fin[3]="2018-04-30";
			total1+=totalDebt(convertirFecha(inicio[3]),convertirFecha(fin[3]))/100;
			Double l=total1;
			while(l>0.0)
			{
				resp[1]+="X";
				l--;
			}
			break;
		case 2:
			Double total2=0.0;
			inicio[0]="2018-05-01";
			fin[0]="2018-05-31";
			total2=totalDebt(convertirFecha(inicio[0]),convertirFecha(fin[0]))/100;
			i=total2;
			while(i>0.0)
			{
				resp[0]+="X";
				i--;
			}

			inicio[1]="2018-06-01";
			fin[1]="2018-06-30";
			total2+=totalDebt(convertirFecha(inicio[1]),convertirFecha(fin[1]))/100;
			j=total2;
			while(j>0.0)
			{
				resp[1]+="X";
				j--;
			}

			inicio[2]="2018-07-01";
			fin[2]="2018-07-31";
			total2+=totalDebt(convertirFecha(inicio[2]),convertirFecha(fin[2]))/100;
			k=total2;
			while(k>0.0)
			{
				resp[2]+="X";
				k--;
			}

			inicio[3]="2018-08-01";
			fin[3]="2018-08-31";
			total2+=totalDebt(convertirFecha(inicio[3]),convertirFecha(fin[3]))/100;
			l=total2;
			while(l>0.0)
			{
				resp[3]+="X";
				l--;
			}
			break;
		case 3:
			Double total3=0.0;
			inicio[0]="2018-09-01";
			fin[0]="2018-09-30";
			total3=totalDebt(convertirFecha(inicio[0]),convertirFecha(fin[0]))/100;
			i=total3;
			while(i>0.0)
			{
				resp[0]+="X";
				i--;
			}

			inicio[1]="2018-10-01";
			fin[1]="2018-10-31";
			total3+=totalDebt(convertirFecha(inicio[1]),convertirFecha(fin[1]))/100;
			j=total3;
			while(j>0.0)
			{
				resp[1]+="X";
				j--;
			}

			inicio[2]="2018-11-01";
			fin[2]="2018-11-30";
			total3+=totalDebt(convertirFecha(inicio[2]),convertirFecha(fin[2]))/100;
			k=total3;
			while(j>0.0)
			{
				resp[2]+="X";
				k--;
			}

			inicio[3]="2018-12-01";
			fin[3]="2018-12-31";
			total3+=totalDebt(convertirFecha(inicio[3]),convertirFecha(fin[3]))/100;
			l=total3;
			while(l>0.0)
			{
				resp[3]+="X";
				l--;
			}
			break;
		}
		return resp;
	}

	//C12 Grafica
	public String[] graficaFine()
	{
		String[] resp=new String[4];
		String[] inicio=new String[12];
		Double[] fines=new Double[12];
		resp[0]="";
		resp[1]="";
		resp[2]="";
		resp[3]="";

		Double total1=0.0;
		/*
		inicio[0]="2018-01-01";
		inicio[1]="2018-02-01";
		inicio[2]="2018-03-01";
		inicio[3]="2018-04-01";
		inicio[4]="2018-05-01";
		inicio[5]="2018-06-01";
		inicio[6]="2018-07-01";
		inicio[7]="2018-08-01";
		inicio[8]="2018-09-01";
		inicio[9]="2018-10-01";
		inicio[10]="2018-11-01";
		inicio[11]="2018-12-01";*/
		NodeList<VOMovingViolations> actual=moving.getFirstNode();

		while(actual!=null)
		{
			int mes=convertirFecha_Hora_LDT(actual.getelem().getTicketIssueDate()).getMonthValue();
			switch(mes)
			{
			case 1:
				fines[0]=actual.getelem().getFine();
				break;
			case 2:
				fines[1]=actual.getelem().getFine();
				break;
			case 3:
				fines[2]=actual.getelem().getFine();
				break;
			case 4:
				fines[3]=actual.getelem().getFine();
				break;
			case 5:
				fines[4]=actual.getelem().getFine();
				break;
			case 6:
				fines[5]=actual.getelem().getFine();
				break;
			case 7:
				fines[6]=actual.getelem().getFine();
				break;
			case 8:
				fines[7]=actual.getelem().getFine();
				break;
			case 9:
				fines[8]=actual.getelem().getFine();
				break;
			case 10:
				fines[9]=actual.getelem().getFine();
				break;
			case 11:
				fines[10]=actual.getelem().getFine();
				break;
			case 12:
				fines[11]=actual.getelem().getFine();
				break;

			}
			actual=actual.getNext();
		}

		switch(numeroCuatrimestre)
		{
		case 1:				
			total1=fines[0]/100;
			Double i=total1;
			while(i>0.0)
			{
				resp[0]+="X";
				i--;
			}

			Double j=total1+fines[1]/100;
			while(j>0.0)
			{
				resp[1]+="X";
				j--;
			}


			Double k=(fines[0]+fines[1]+fines[2])/100;
			while(k>0.0)
			{
				resp[2]+="X";
				k--;
			}

			Double l=(fines[0]+fines[1]+fines[2]+fines[3])/100;
			while(l>0.0)
			{
				resp[3]+="X";
				l--;
			}
			break;
		case 2:

			i=fines[4]/100;
			while(i>0.0)
			{
				resp[0]+="X";
				i--;
			}

			j=fines[4]/100+fines[5]/100;
			while(j>0.0)
			{
				resp[1]+="X";
				j--;
			}


			k=(fines[4]+fines[5]+fines[6])/100;
			while(k>0.0)
			{
				resp[2]+="X";
				k--;
			}

			l=(fines[4]+fines[5]+fines[6]+fines[7])/100;
			while(l>0.0)
			{
				resp[3]+="X";
				l--;
			}
			break;
		case 3:
			i=fines[8]/100;
			while(i>0.0)
			{
				resp[0]+="X";
				i--;
			}

			j=fines[8]/100+fines[9]/100;
			while(j>0.0)
			{
				resp[1]+="X";
				j--;
			}


			k=(fines[8]+fines[9]+fines[10])/100;
			while(k>0.0)
			{
				resp[2]+="X";
				k--;
			}

			l=(fines[8]+fines[9]+fines[10]+fines[11])/100;
			while(l>0.0)
			{
				resp[3]+="X";
				l--;
			}
			break;
		}
		return resp;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)

	{

		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));

	}
}

