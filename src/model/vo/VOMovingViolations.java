package model.vo;

import java.util.Comparator;

import java.util.Date;

import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;



/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Iterable<VOMovingViolations>, Comparable<VOMovingViolations> 
{

	private int objectId  ;
	private String location , penalty1 , penalty2, accidentIndicator , violationDes , streetSegId , addressId , violationCode;
	private String ticketIssue;
	private double fineamt, totalPaid;
	public VOMovingViolations(String pLocation ,double pFine, String pTicketIssue , String pAccidentIndicator , String pViolationDes , String pViolationCode , String pStreetId , String pAddressId , int pObecjt , double pTotal , String pPenalty1 , String pPenalty2)
	{
		location = pLocation;
		ticketIssue = pTicketIssue;
		fineamt = pFine;
		violationCode = pViolationCode;
		/**
{
DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
DateTime dt = dtf.parseDateTime(pTicketIssue);
ticketIssue = dt.toDate();
}
catch (Exception e)
{
if (pTicketIssue.indexOf("T")!=-1 && pTicketIssue.indexOf("T") != 0)
{
String dateWorkaround = pTicketIssue.substring(0, pTicketIssue.indexOf("T"));
DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
DateTime dt = dtf.parseDateTime(dateWorkaround);
ticketIssue = dt.toDate();
}
}
		 */
		accidentIndicator = pAccidentIndicator;
		violationDes = pViolationDes;
		objectId = pObecjt;
		totalPaid = pTotal;
		streetSegId = pStreetId;
		addressId = pAddressId;
		penalty1 = pPenalty1;
		penalty2 = pPenalty2;
	}
	public VOMovingViolations()
	{
		
	}
	/**
	 * @return id - Identificador Ãºnico de la infracciÃ³n
	 */
	public int objectId() {
		return objectId;
	} 
	/**
	 * @return location - DirecciÃ³n en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracciÃ³n .
	 */
	public String getTicketIssueDate() {
		return ticketIssue;
	}
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagÃ³ el que recibiÃ³ la infracciÃ³n en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	public double getFine()
	{
		return fineamt;
	}
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
	/**
	 * @return description - DescripciÃ³n textual de la infracciÃ³n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDes;
	}
	public String getViolationCode()
	{
		return violationCode;
	}
	public String getStreetSegId() {
		return streetSegId;
	}
	public String getAddressId() {
		return addressId;
	}


	public String getPenalty1()
	{
		return penalty1;
	}
	public String getPenalty2() {
		return penalty2;
	}
	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO Auto-generated method stub
		if (o.objectId() == this.objectId() )
		{
			return 0;
		}
		else if (o.objectId() < this.objectId()) 
		{
			return 1;
		}
		else
			return -1;
	}


	public static class ComparadorXfecha implements Comparator<VOMovingViolations>
	{
		public int compare(VOMovingViolations VO1,VOMovingViolations VO2)
		{
			return VO1.getTicketIssueDate().compareToIgnoreCase(VO2.getTicketIssueDate());
		}
	}
	
	@Override
	public Iterator<VOMovingViolations> iterator() {
		return (Iterator<VOMovingViolations>) this;
	}
	public String toString()
	{
		return "VOMovingViolation : { id : " + objectId + ", location : " + location + " , finemat :" + fineamt + " , penalty1  :" + penalty1 +  " , penalty2 :" + penalty2 +  " , ticketIssue :" + ticketIssue +  " ,  :" + violationCode    +" ,  total paid : " + totalPaid + " , Accidente Indicator : " + accidentIndicator + " , violationDes : " + violationDes + " , Street : " + streetSegId + " , Address : " + addressId + "}"  ;
	}
}
