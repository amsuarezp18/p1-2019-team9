package model.vo;

import java.util.Iterator;

public class VOViolationCode implements Iterable<VOViolationCode>, Comparable<VOViolationCode> {

	private double avgFineAmt;
	private String violationCode;
	
	public VOViolationCode(String pViolationCode , double pAvgFineAmt)
	{
		avgFineAmt = pAvgFineAmt;
		violationCode = pViolationCode;
	}
	
	public String getViolationCode() {
		return violationCode;
	}
	
	public double getAvgFineAmt() {
		return avgFineAmt;
	}

	@Override
	public int compareTo(VOViolationCode o) {
		// TODO Auto-generated method stub
		return violationCode.compareToIgnoreCase(getViolationCode());
	}

	@Override
	public Iterator<VOViolationCode> iterator() {
		// TODO Auto-generated method stub
		return (Iterator<VOViolationCode>) this;
	}
	
	public String toString()
	{
		return " VOViolationCode { violationCode : " + violationCode + " , avgFineAmt : " + avgFineAmt +  " } " ; 
	}
}
