package model.vo;

import java.util.Iterator;

import model.data_structures.QueueList;

public class VODaylyStatistic  implements Iterable<VODaylyStatistic> , Comparable<VODaylyStatistic>
{
	private QueueList<VODaylyStatistic> queueLinkedList;
	private String fecha;
	private int numAccidentes,numInfracciones;
	private double FINEAMT;
	public VODaylyStatistic(String pFecha, int pNumAccidentes, int pNumInfracciones, double pFINEAMT)
	{

		fecha = pFecha;		
		numAccidentes = pNumInfracciones;
		numInfracciones =pNumInfracciones;
		FINEAMT=pFINEAMT;
	}


	@Override
	public int compareTo(VODaylyStatistic o) {

		return fecha.compareTo(o.darFecha());
	}

	public String darFecha()
	{
		return fecha;
	}

	public int darNumeroAccidentes()
	{
		return numAccidentes;
	}
	public int darNumeroInfracciones()
	{
		return numInfracciones;
	}

	public double darSumaTotal()
	{
		return FINEAMT;
	}
	@Override
	public Iterator<VODaylyStatistic> iterator() {

		return iterator();
	}
	//TODO
	public String toString()
	{
		return fecha.toString()+"_"+ numAccidentes+"_"+numInfracciones+"_"+FINEAMT;
	}
}
