package model.logic;



import java.io.Reader;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.NodeList;
import model.data_structures.QueueList;
import model.data_structures.StackList;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;

import java.util.Iterator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class MovingViolations {

	private LinkedList<VOMovingViolations> moving;
	private QueueList<VOMovingViolations> movingQueue;

	private LinkedList<Long> ids;
	private LinkedList<VOMovingViolations> verifyObjectIDIsUnique;
	private int repetidos;

	public LinkedList<VOMovingViolations> getVerify()
	{
		return verifyObjectIDIsUnique;
	}
	public LinkedList<VOMovingViolations> getMoving() 
	{
		return moving;
	}
	public QueueList<VOMovingViolations> getQueue()
	{
		return movingQueue;
	}

	public int getRepetidos()
	{
		return repetidos;
	}
	public LinkedList<Long> objectid() 
	{
		return ids;
	}

	public void setMoving(LinkedList<VOMovingViolations> moving) {
		this.moving = moving;
	}


	private static final String[] attributes = {"OBJECTID","ROW_","LOCATION","ADDRESS_ID","STREETSEGID","XCOORD","YCOORD","TICKETTYPE","FINEAMT","TOTALPAID","PENALTY1","AGENCYID", "PENALTY2","ACCIDENTINDICATOR","TICKETISSUEDATE","VIOLATIONCODE","VIOLATIONDESC","ROW_ID"};


	public MovingViolations()
	{
		moving = new LinkedList<VOMovingViolations>();
		movingQueue = new QueueList<VOMovingViolations>();
		repetidos = 0;
		ids = new LinkedList<Long>();
		verifyObjectIDIsUnique = new LinkedList<VOMovingViolations>();

	}


	public void loadMovingViolations(String movingViolationsFile, boolean otroAtributo) {

		int linea = 0;
		try {
			Reader reader = Files.newBufferedReader(Paths.get(movingViolationsFile));
			CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);


			for (CSVRecord csvRecord : csvParser) {
				linea++;

				//El CSV ESTÃ� MAL FORMATEADO, NO ENCUENTRA CAMPO 2, POR ESO EXCEPCIÃ“N "2".
				//TODOS LOS CAMPOS QUEDAN EN POSICIÃ“N 0.
				// POSIBLE SOLUCION -> Verificar si el campo 0 (OBJECTID) tiene comas, si sÃ­ entonces saltar la lectura de csvRecord y asignar los valores desde ahi.
				// EN ESTAS LINEAS SALE EL ERROR:   linea == 841 || linea == 911 || linea == 1195 ... 
				// ERROR EN LAS FECHAS 2018-12-2500... Cambiar la fecha a la original
				if (linea == 1)
					continue;

				{
					// Accessing Values by Column Index
					String OBJECTID =  csvRecord.get(0);
					if(OBJECTID.contains(","))
						continue;

					String LOCATION = csvRecord.get(2);

					String TOTALPAID = csvRecord.get(9);

					String ACCIDENTINDICATOR = csvRecord.get(12);

					String TICKETISSUEDATE =  !otroAtributo ? csvRecord.get(13).toString() : csvRecord.get(14);

					String VIOLATIONDESC = !otroAtributo ? csvRecord.get(15).toString() :  csvRecord.get(16)  ;

					String ADDRESS_ID = csvRecord.get(3); 

					String  STREETSEGID = csvRecord.get(4);  
					String FINEAMT = csvRecord.get(8);
					String VIOLATIONCODE = !otroAtributo ? csvRecord.get(14).toString() :  csvRecord.get(15);
					String PENALTY1 = csvRecord.get(10);
					String PENALTY2 = csvRecord.get(11);
					//System.out.println(OBJECTID + "," + LOCATION + "," + TOTALPAID + "," + ACCIDENTINDICATOR + "," + TICKETISSUEDATE + "," + VIOLATIONDESC + "," + ADDRESS_ID + "," + STREETSEGID);
					VOMovingViolations listaa = new VOMovingViolations(LOCATION,Double.parseDouble(FINEAMT), TICKETISSUEDATE, ACCIDENTINDICATOR, VIOLATIONDESC, VIOLATIONCODE,STREETSEGID,ADDRESS_ID, Integer.parseInt(OBJECTID), Double.parseDouble(TOTALPAID), PENALTY1 , PENALTY2 ) ;
					moving.add(listaa);
					movingQueue.enqueue(listaa);
				}
			}

		}
		catch (Exception e)
		{
			System.out.println("AQUI - linea" + linea);
			System.out.println(e.getStackTrace().toString());
			System.out.println(e.getMessage());
		}

	}
	//A1 dos
	public boolean ve()
	{   
		int rome  = moving.getSize()-1;
		boolean resp = true;
		for( int i = 0 ; i < rome/2 ; i++)
		{
			for ( int j = rome ; j > rome/2   ; j--)
			{
				if( moving.getElement(i).compareTo(moving.getElement(j)) == 0)
				{
					resp = false;
				}
			}
		}
		return resp;
	}
	
	//A1 Angela Suarez
	
	public boolean verificar()
	{

		int repetido = 0;
		int rome  = moving.getSize();
		int posicionEncontrado = 0;
		boolean encontrado = false;
		int j = 1;
		for (int i = 0; i < rome; i++)
		{
			j = encontrado == true ? posicionEncontrado : i+j;
			for (; j< rome; j++)
			{
				if ( moving.getElement(i).objectId() == moving.getElement(j).objectId())
				{
					System.out.println("Repetido: " + moving.getElement(i).objectId());
					repetido++;
					posicionEncontrado = j;
					encontrado = true;
					break;
				}
				encontrado = false;
			}
			if (!encontrado)
				System.out.println("No repetido: " + moving.getElement(i).objectId());
			//j = i;
		}

		return repetido == 0 ? false : true;
	}




	public IQueue <VODaylyStatistic> getDailyStatistics () {
		//QueueList<VOMovingViolations> cola= (QueueList<VOMovingViolations>)movingVQueue;
		QueueList <VODaylyStatistic> dailyStatistic= new QueueList<VODaylyStatistic>();

		for(VOMovingViolations infra: moving)
		{
			String fecha= infra.getTicketIssueDate();
			int numAccidentes=(infra.getAccidentIndicator()=="Yes")?1:0;
			int numInfracciones=1;
			double total=infra.getTotalPaid();
			for(VOMovingViolations infraCopia: moving)
			{
				//Falta revisar lo del día
				if(convertirFecha(infraCopia.getTicketIssueDate()).equals(convertirFecha(infra.getTicketIssueDate())))
				{
					numAccidentes++;
					numInfracciones++;
					total+= infra.getTotalPaid();
				}
			}
			VODaylyStatistic dayStatistics= new VODaylyStatistic(fecha,numAccidentes,numInfracciones,total);
			dailyStatistic.enqueue(dayStatistics);
		}
		return dailyStatistic;

	}
	
	public IStack <VOMovingViolations> nLastAccidents(int n) {
		StackList <VOMovingViolations> resp= (StackList<VOMovingViolations>) new StackList<VOMovingViolations>();
		//LinkedList<VOMovingViolations> copiaMoving = new LinkedList<VOMovingViolations>();
		//copiaMoving=moving;
		NodeList<VOMovingViolations> actual= moving.getFirstNode();
		int i=n;
		while(i>0&&actual!=null)
		{
			if(actual.getelem().getAccidentIndicator().equalsIgnoreCase("Yes"))
			{
				resp.push(actual.getelem());
				System.out.println(actual.toString());
				i--;
			}
		}

		return resp;
	}
	
	//1B
	public IQueue<VOViolationCode> violationCodesByFineAmt(double limiteInf5, double limiteSup5)
	{
		IQueue<VOViolationCode> resp= new QueueList<VOViolationCode>();
		VOMovingViolations actual=movingQueue.dequeue();

		while(!movingQueue.isEmpty())
		{
			double promedio=0;
			int total=0;
			String descripActual="";
			VOMovingViolations siguiente=movingQueue.dequeue();
			while(actual!=null&& siguiente!=null)
			{
				descripActual=actual.getViolationDescription();
				String descripSig=siguiente.getViolationDescription();
				if(descripActual.equalsIgnoreCase(descripSig))
				{
					promedio+=actual.getFine();
					total++;
				}
				else
				{
					movingQueue.enqueue(siguiente);
				}
			}
			promedio=promedio/total;
			if(limiteInf5<promedio&& promedio<limiteSup5)
			{
				VOViolationCode t= new VOViolationCode(descripActual, promedio);
				resp.enqueue(t);
			}			

			actual=movingQueue.dequeue();
		}

		return resp;
	}

	
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

}




