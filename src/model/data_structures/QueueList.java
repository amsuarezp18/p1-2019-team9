
package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class QueueList<T extends Comparable<T>> implements IQueue<T>
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Tamaño del nodo.
	 */
	private int size;

	/**
	 * Referencia a primer nodo.
	 */

	NodeList<T> first;

	/**
	 * Referencia a ultimo nodo.
	 */
	NodeList<T> last;


	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	public QueueList()
	{
		size = 0;
		first = null; 
		last = null;
	}

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------


	/**
	 * Indica si la lista esta vacia 
	 * @return true si no hay ningun elemento, false de lo contrario
	 */
	@Override
	public boolean isEmpty() {
		return size == 0 ? true: false;
	}

	/**
	 * Devuelve el tamaño de la lista
	 * @return tamaño de la lista 
	 */
	@Override
	public int size() {
		return size;
	}


	/**
	 * Quita y retorna el elemento agregado menos recientemente
	 * @return el elemento agregado menos recientemente
	 */
	@Override
	public T dequeue() {


		T value = null;
		if( first != null)
		{
			value = first.getelem();
			first = first.getNext();
			size--;
		}

		return value;

	}

	/**
	 * Clase interna iteradora
	 */


	@Override
	public Iterator<T> iterator() {

		Iterator<T> iterator = new IteratorQueue();
		return iterator;
	}


	private class IteratorQueue implements Iterator<T>
	{
		private NodeList<T> proximo = first;

		@Override
		public boolean hasNext() {

			return proximo != null?true:false;


		}

		@Override
		public T next() {

			if( proximo == null)
			{
				throw new NoSuchElementException("no hay proximo");
			}
			T elemento = proximo.getelem();
			proximo = (NodeList<T>) proximo.getNext();
			return elemento;
		}

	}
	/**
    Inserta un nuevo elemento en la Cola
	 * @param t el nuevo elemento que se va ha agregar
	 */
	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		NodeList<T> aux = new NodeList<T>();
		aux.setElement(t);

		if( first == null)
		{
			first = aux;
			last = aux;
		}
		else
		{
			last.setNext(aux);
			last = last.getNext();
		}
		size++;
	}





}
