package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.csv.CSVRecord;

import model.vo.VOMovingViolations;

public class LinkedList<T extends Comparable<T>> implements ILinkedList<T > {
// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
/**
     * Primer nodo.
     */
	private NodeList<T> first;
/**
     * ultimo nodo.
     */
	private NodeList<T> last;
/**
     * Primer nodo.
     */
	private int size;
/**
     * Primer nodo.
     */
	private NodeList<T> current;
// -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

public LinkedList()
{
size =0;
first = null;
setLast(null);
current = first;
}

// -----------------------------------------------------------------
    // MÃ©todos
    // -----------------------------------------------------------------
/**


	

     * Devuelve el siguiente elemento en la lista
     * @return elemento T siguiente
     */
public T next() {
current = current.getNext();
return  current.getelem();
}


public NodeList<T> getFirstNode()
	{
		return first;
	}

/**
     * Devuelve el anteior elemento de la lista 
     * @return elemento T anteior
     */
public T previus() {
current = current.getPrevius();
return  current.getelem();
}

/**
     * Devuelve el tamaÃ±o de la lista
     * @return tamaÃ±o de la lista 
     */
@Override
public int getSize() {
return size;
}

/**
     * Devuelve el elemento actual de la lista
     * @return elemento T actual
     */
public T getCurrent() {
return current.getelem();
}

public boolean isEmpty() {
return size == 0 ? true: false;
}

/**
     * Indica si un elemento esta repetido
     * correquisito para aÃ±adir elementos 
     * @param T elemento
     * @return true si se logro encontrar un elemento T igual a alguno en la lista
     */

public boolean repeated(T dato) {
    NodeList<T> nodos = first.getNext();
    while ( nodos.hasNext())
    {
    if ( nodos.getelem().compareTo(dato)== 0)
    {
    return false;
   
    }
     nodos = nodos.getNext();
    }
 
    return true;
}

/**
     * Elimina un elemento de la lista dado por parametro
     * @param T elem
     * @return true si se logro eliminar el elemento T
     */
public boolean delete(T elem) {
boolean resp = false;
NodeList<T> pro = first;
if( pro.getelem().equals(elem))
{
first = pro.getNext();
size--;
resp = true;
}
while( pro.hasNext())
{
if( pro.getelem().equals(elem))
           {
NodeList<T> temp = pro.getNext();
pro.getPrevius().setNext(temp);
size--;
resp =  true;
         }
pro = pro.getNext();
}
   return resp;
}

/**
     * AÃ±ade un elemento a la lista de primeras, si no existe primero,
     * este se convierte en el primero
     * @param T elem a agregar
     * @return true si se logro agregar el elemento T
     */

public boolean add(T elem) {
NodeList<T> temp = new NodeList<T>(elem, null, null);
boolean resp = false;
if( first == null)
{
first = temp;
setLast(temp);
size++;
}
else
{

first.setPrevius(temp);
temp.setNext(first);
first = temp;
resp = true;
size++;
}
return resp;
}
public T addInorder(T elemp)
{
NodeList<T> aux = new NodeList<T>();
aux.setElement(elemp);
if( first == null)
{
first = aux;
last = aux;
}
else
{
NodeList<T> mo = first;
while ( mo.getelem().compareTo(aux.getelem()) > 0 && mo != last)
{
mo = mo.getNext()!=null ? mo.getNext() :last;
}
if( mo.getelem().compareTo(aux.getelem()) < 0 && mo.getelem().compareTo(first.getelem())==0)
{
mo.setPrevius(aux);
aux.setNext(mo);
first = aux;
}
else if ( mo.getelem().compareTo(aux.getelem())==0)
{
NodeList<T> nextMo = mo.getNext();
mo.setNext(aux);
aux.setPrevius(mo);
aux.setNext(nextMo);
}
else if(mo.getelem().compareTo(aux.getelem()) <0 )
{
NodeList<T> previoustMo = mo.getPrevius();
mo.setPrevius(aux);
aux.setNext(mo);
aux.setPrevius(previoustMo);
previoustMo.setNext(aux);
}
else
{
mo.setNext(aux);
aux.setPrevius(mo);
last = aux;
}
}
size ++;
return null;
}

public T addInOrdeRRr(T pelem)
{
NodeList<T> aux = new NodeList<T>();
aux.setElement(pelem);
if ( first == null)
{
first = aux;
last = first;
}
else
{
NodeList<T> mo = first;
while(mo.getelem().compareTo(aux.getelem())>0 && mo != last )
{
mo = mo.getNext()!= null ? mo.getNext() : last;
}
if( mo.getelem().compareTo(aux.getelem())== 0)
{
return mo.getelem();
}
else if(mo.getelem().compareTo(aux.getelem()) < 0 && mo.getelem().compareTo(first.getelem() )== 0)
{
mo.setPrevius(aux);
aux.setNext(mo);
first = aux;
}
else if(mo.getelem().compareTo(aux.getelem()) < 0)
{
NodeList<T> pre = mo.getPrevius();
mo.setPrevius(aux);
aux.setNext(mo);
aux.setPrevius(pre);
pre.setNext(aux);
}
else
{
mo.setNext(aux);
aux.setPrevius(mo);
last = aux;
}
}
size++;
return null;
}
/**
     * AÃ±ade un elemento a la lista de primeras, si no existe primero,
     * este se convierte en el primero
     * @param T elem a agregar
     * @return true si se logro agregar el elemento T
     */
public boolean addAtEnd(T elem) {
NodeList<T> pro = first;
NodeList<T> aux = new NodeList<T>(elem, null, null);
for ( int i = 0; i <  size ; i++)
{
if(first == null)
{
setLast(aux);
first =  aux;
size++;
return true;
}
if( !pro.hasNext())
{
pro.setNext(aux);
aux.setPrevius(pro);
setLast(aux);
size++;
return true;
}
pro = pro.getNext();
}
return false;
        
}

/**
     * AÃ±ade un elemento a la lista de primeras, si no existe primero,
     * este se convierte en el primero
     * @param T elem a agregar
     * @return true si se logro agregar el elemento T
     */
public boolean addAtK(T elem, int nii) {
NodeList<T> pro = first;
NodeList<T> aux = new NodeList<T>(elem, null, null); 
for( int i = 0; i <= size; i++)
{
if( i == nii)
{
NodeList<T> temporal = pro.getNext();
pro.setNext(aux);
aux.setPrevius(pro);
aux.setNext(temporal);
temporal.setPrevius(aux);
size++;
return true;
}
pro = pro.getNext();
}
return false;
}

/**
     * Obtiene el elemento buscado
     * @param pPosicion 
     * @return el elemento en la posicion T 
     */
@Override
public T getElement(int pos) 
{
	if (pos < 0)
		return null;
	NodeList<T> chk = first;
	if (chk!= null) 
	{
		for (int i=0;i<getSize();i++)
		{
			if(chk.getNext()==null && i+1==pos)
			{
				return null;
			}
			else if(i==pos)
			{
				return chk.getelem();
			}

			chk = chk.getNext();
		}

	}
	return null;
}

/**
     * elimina un elemento en la poscion T
     * @param T elemento , il posicion
     * @return el elemento en la posicion T 
     */
public T deleteAtKObject( int iL) {
NodeList<T> pro = first;
String orden = "";
for (int i = 0; i < size; i++) {
orden += pro.getelem() + "->";
if (pro.hasNext())
pro = pro.getNext();
}
System.out.println(orden);
pro = first;
for( int i = 0; i < size; i++)
{
System.out.println("Actual: " + pro.getelem());
if( i == iL)
{
NodeList<T> elBorrado = pro;
if( pro.hasPrevious())
{
NodeList<T> temp = pro.getNext();
pro.getPrevius().setNext(temp);
size--;
}
else
{
first = pro.getNext();
size--;
}
System.out.println("El borrado: " + pro.getelem());
System.out.println("Fin borrado");
return elBorrado.getelem();
}
pro = pro.getNext();
}
return null;

       
}
/**
     * elimina un elemento en la poscion T
     * @param T elemento , il posicion
     * @return el elemento en la posicion T 
     */
public boolean deleteAtK( int iL) {
NodeList<T> pro = first;
for( int i = 0; i < size; i++)
{
if( i == iL)
{
if( pro.hasPrevious())
{
NodeList<T> temp = pro.getNext();
pro.getPrevius().setNext(temp);
size--;
}
else
{
size--;
first = pro.getNext();
}
return true;
}
pro = pro.getNext();
}
return false;

       
}
public  void shuffleList()
  {
    // If running on Java 6 or older, use `new Random()` on RHS here
  
    for (int i = 0; i < this.getSize(); i++)
    {
    int rnd = ((int) ((this.getSize()-i)*Math.random()));
    this.addAtEnd(this.deleteAtKObject(rnd));
    }
  }
public T isrepeated(T elem)
{
if (size == 1)
return null;
T found = null;
int mitad = size/2;
if(elem.compareTo(this.getElement(size/2))==0)
{
found = this.getElement(size/2);
return found;
}
else if ( elem.compareTo(this.getElement(size/2)) < 0)
{
    for (int i=0; i<size/2;i++)
    {
    if (this.getElement(i).compareTo(elem) == 0)
    {
    found = elem;
    return found;
    }

   
    }
 
}
else
{
for (int i=size/2; i<size;i++)
    {
if (this.getElement(i).compareTo(elem) == 0)
    {
    found = elem;
    return found;
    }

    }
 
}
return found;
 
}
 
/**
     * Clase iteradora
     */
private Iterator<T> iterator;
@Override
public Iterator<T> iterator() {
iterator = new IteratorLinkedList();
return iterator;
}
/**
* @return the last
*/
public NodeList<T> getLast() {
return last;
}

/**
* @param last the last to set
*/
public void setLast(NodeList<T> last) {
this.last = last;
}





public class IteratorLinkedList implements Iterator<T>
{
private NodeList<T> proximo = first;

@Override
public boolean hasNext() {
return proximo != null?true:false;
}

@Override
public T next() {
if( proximo == null)
{
throw new NoSuchElementException("no hay proximo");
}
T elemento = proximo.getelem();
proximo = (NodeList<T>) proximo.getNext();
return elemento;
}
}









}

