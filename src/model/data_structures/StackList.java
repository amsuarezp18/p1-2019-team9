package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import com.sun.org.apache.bcel.internal.classfile.Node;


public class StackList<T extends Comparable<T>> implements IStack<T> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Tamaño del nodo.
	 */
	private	int size;

	/**
	 * Referencia  a primer nodo.
	 */
	NodeList<T> first;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------

	public StackList()
	{
		size  = 0; 
		first = null;
	}

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------


	/**
	 * Indica si la lista esta vacia 
	 * @return true si no hay ningun elemento, false de lo contrario
	 */
	@Override
	public boolean isEmpty() {
		return size == 0? true: false;
	}

	/**
	 * Devuelve el tamaño de la lista
	 * @return tamaño de la lista 
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Inserta un elemento T en la pila
	 * @param elemento a agregar
	 */
	@Override
	public void push(T t) {
		NodeList<T> aux = new NodeList<T>(t, null , null);

		if( first == null)
		{
			first = aux;
		}
		else
		{
			first.setPrevius(aux);
			aux.setNext(first);
			first = aux;
		}

		size++;
	}
	//Hace push ordenadamente de acuerdo al compare por parametro
	public void pushConCompare(T t, Comparator<T> comparator)
	{
		NodeList<T> aux = new NodeList<T>(t, null , null);
		if(first==null)			
		{
			first=aux;
		}
		else
		{
			if(comparator.compare(first.getelem(),aux.getelem())<0)
			{
				first.setPrevius(aux);
				aux.setNext(first);
				first = aux;
			}
			else
			{
				NodeList<T> mo = first;
				while(comparator.compare(first.getelem(),mo.getelem())>0&&mo.getNext()!=null)
				{
					mo = mo.getNext();
					//mo=mo.getNext()!= null ? mo.getNext() : last;
				}
				if(comparator.compare(first.getelem(),aux.getelem())<0)
				{
					NodeList<T> previoustMo = mo.getPrevius();
					mo.setPrevius(aux);
					aux.setNext(mo);
					aux.setPrevius(previoustMo);
					previoustMo.setNext(aux);
				}
				else
				{
					mo.setNext(aux);
					aux.setPrevius(mo);
				}
			}
		}
		size++;
	}

	/**
	 * Remueve el elemento T que se agrego recientemente
	 * @return el elemento T mas reciente que se inserto
	 */
	@Override
	public T pop() {

		T valor = null;
		if( first != null)
		{
			valor = first.getelem();
			first = first.getNext();
			size --;
		}

		return valor;
	}


	/**
	 * Clase interna iteradora
	 */


	@Override
	public Iterator<T> iterator() {

		Iterator<T> iterator = new IteratorStack();
		return iterator;
	}


	private class IteratorStack implements Iterator<T>
	{
		private NodeList<T> proximo = first;

		@Override
		public boolean hasNext() {

			return proximo != null?true:false;


		}

		@Override
		public T next() {

			if( proximo == null)
			{
				throw new NoSuchElementException("no hay proximo");
			}
			T elemento = proximo.getelem();
			proximo = (NodeList<T>) proximo.getNext();
			return elemento;
		}

	}
}
